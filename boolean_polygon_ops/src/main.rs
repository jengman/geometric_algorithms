extern crate ggez;
extern crate rand;

use std::fs::File;
use std::io::{self, Read};

use ggez::conf;
use ggez::event;
use ggez::graphics::{self, DrawMode, Point2};
use ggez::{Context, GameResult};
use rand::random;


///////////////
// ALGORITHM //
///////////////

fn get_line_function(p1: &Point2, p2: &Point2) -> Option<Box<Fn(f32) -> f32>> {
    // Generates a function which describes the line from p1 to p2 as y(x) = kx+m
    if p1.x == p2.x {
        return None;
    }

    let k = (p2.y - p1.y) / (p2.x - p1.x);
    let m = p1.y - k * p1.x;

    Some(Box::new(move |x| k * x + m))
}

fn is_left_of(p1: &Point2, p2: &Point2, other: &Point2) -> bool {
    // Checks whether next is a left turn based on two points making up a line
    // The order of p1 and p2 is important!

    match (get_line_function(&p1, &p2), p1.x < p2.x) {
        (Some(f), true) => f(other.x) < other.y,

        (Some(f), false) => f(other.x) > other.y,

        (None, _) => p1.x < other.x,
    }
}

fn nth<'a, T>(slc: &'a [T], index: usize) -> &'a T {
    &slc[index]
}


fn union(p1: &[Point2], p2: &[Point2]) -> Vec<Point2> {
    println!("UNION NOT YET IMPLEMENTED");

    Vec::new()
}

fn intersection(p1: &[Point2], p2: &[Point2]) -> Vec<Point2> {
    println!("INTERSECTION NOT YET IMPLEMENTED");

    Vec::new()
}

fn difference(p1: &[Point2], p2: &[Point2]) -> Vec<Point2> {
    println!("DIFFERENCE NOT YET IMPLEMENTED");

    Vec::new()
}

///////////////
//    GUI    //
///////////////

enum ActivePoly {
    A,
    B,
}

struct State {
    instr:  [graphics::Text; 6],
    active: ActivePoly,
    poly_a:  Vec<Point2>,
    poly_b:  Vec<Point2>,
    lines:  Vec<Point2>,
    info:   [graphics::Text; 2],
}

impl State {
    fn new(ctx: &mut Context) -> GameResult<State> {
        let font = graphics::Font::default_font()?;

        let instr = [
            graphics::Text::new(ctx, "[U]nion", &font)?,
            graphics::Text::new(ctx, "[I]ntersection", &font)?,
            graphics::Text::new(ctx, "[D]ifference", &font)?,
            graphics::Text::new(ctx, "[S]wap hull", &font)?,
            graphics::Text::new(ctx, "[R]andomize", &font)?,
            graphics::Text::new(ctx, "[C]lear", &font)?,
        ];

        let info = [
            graphics::Text::new(ctx, "ACTIVE", &font)?,
            graphics::Text::new(ctx, &format!("Verts: {:05}", 0), &font)?,
        ];

        let s = State {
            instr:  instr,
            active: ActivePoly::A,
            poly_a:  Vec::new(),
            poly_b:  Vec::new(),
            lines:  Vec::new(),
            info:   info,
        };

        Ok(s)
    }

    fn push_active(&mut self, vert: Point2) {
        match self.active {
            ActivePoly::A => self.poly_a.push(vert),

            ActivePoly::B => self.poly_b.push(vert),
        }
    }

    fn append_active(&mut self, verts: &[Point2]) {
        match self.active {
            ActivePoly::A => self.poly_a.extend_from_slice(verts),

            ActivePoly::B => self.poly_b.extend_from_slice(verts),
        }
    }

    fn update_info(&mut self, ctx: &mut Context) {
        let font = graphics::Font::default_font().unwrap();
        self.info[1] = graphics::Text::new(ctx,
                                           &format!("Verts: {:05}", self.poly_a.len() + self.poly_b.len()),
                                           &font).unwrap();

    }

}

impl event::EventHandler for State {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);

        // Polygon A has blue vertices
        graphics::set_color(ctx, graphics::Color::new(0.173, 0.51, 0.788, 1.0))?;

        for v in &self.poly_a {
            let p = world_to_screen(ctx, v);
            graphics::circle(ctx, DrawMode::Fill, p, 2.0, 0.1)?;
        }

        // Polygon B has orange vertices
        graphics::set_color(ctx, graphics::Color::new(0.976, 0.412, 0.0, 1.0))?;

        for v in &self.poly_b {
            let p = world_to_screen(ctx, v);
            graphics::circle(ctx, DrawMode::Fill, p, 2.0, 0.1)?;
        }

        // Reset to white
        graphics::set_color(ctx, graphics::Color::new(1.0, 1.0, 1.0, 1.0))?;

        if self.lines.len() > 1 {
            let lines: Vec<_> = self.lines.iter().map(|p| world_to_screen(ctx, p)).collect();
            graphics::line(ctx, &lines, 2.0)?;
        }

        // Draw all instructions
        let mut y_offset = 4.0;
        for e in &self.instr {
            let pos = graphics::Point2::new(4.0, y_offset);
            graphics::draw(ctx, e, pos, 0.0)?;

            y_offset += e.height() as f32 + 2.0;
        }

        let mut y_offset = 4.0;
        let (screen_w, _) = graphics::get_size(ctx);

        { // Draw ACTIVE
            let e = &self.info[0];
            let pos = graphics::Point2::new((screen_w - e.width() - 4) as f32, y_offset);

            match self.active {
                ActivePoly::A =>
                    graphics::set_color(ctx, graphics::Color::new(0.173, 0.51, 0.788, 1.0))?,

                ActivePoly::B =>
                    graphics::set_color(ctx, graphics::Color::new(0.976, 0.412, 0.0, 1.0))?,
            };

            graphics::draw(ctx, e, pos, 0.0)?;

            // Reset to white
            graphics::set_color(ctx, graphics::Color::new(1.0, 1.0, 1.0, 1.0))?;

            y_offset += e.height() as f32 + 2.0;
        }

        { // Draw Verts: X
            let e = &self.info[1];
            let pos = graphics::Point2::new((screen_w - e.width() - 4) as f32, y_offset);

            graphics::draw(ctx, e, pos, 0.0)?;

            y_offset += e.height() as f32 + 2.0;
        }

        graphics::present(ctx);
        Ok(())
    }

    fn mouse_button_down_event(
        &mut self,
        ctx: &mut Context,
        button: event::MouseButton,
        x: i32,
        y: i32,
    ) {
        if button == event::MouseButton::Left {
            let p = screen_to_world(ctx, &Point2::new(x as f32, y as f32));

            self.push_active(p);

            self.update_info(ctx);
        }
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: event::Keycode,
        _mod: event::Mod,
        _rpt: bool,
    ) {
        match keycode {
            event::Keycode::U => {
                self.lines = union(&self.poly_a, &self.poly_b);
            }

            event::Keycode::I => {
                self.lines = intersection(&self.poly_a, &self.poly_b);
            }

            event::Keycode::D => {
                self.lines = difference(&self.poly_a, &self.poly_b);
            }

            event::Keycode::S => {
                self.active = match self.active {
                    ActivePoly::A => ActivePoly::B,

                    ActivePoly::B => ActivePoly::A,
                };
            }

            event::Keycode::C => {
                match self.active {
                    ActivePoly::A => self.poly_a = Vec::new(),

                    ActivePoly::B => self.poly_b = Vec::new(),
                };

                self.lines = Vec::new();
            }

            event::Keycode::R => {
                // TODO: Relax the randomization slightly
                let (screen_w, screen_h) = graphics::get_size(ctx);
                let w = screen_w as f32;
                let h = screen_h as f32;

                let mut verts: Vec<_> = (0..)
                    .take(20)
                    .map(|_| {
                        (
                            random::<f32>() * w * 0.8 + w * 0.1,
                            random::<f32>() * h * 0.8 + h * 0.1,
                        )
                    })
                    .map(|(x, y)| screen_to_world(ctx, &Point2::new(x, y)))
                    .collect();

                self.append_active(&verts);
                self.lines = Vec::new();

                self.update_info(ctx);
            }

            event::Keycode::Q => ctx.quit().unwrap(),

            event::Keycode::Escape => ctx.quit().unwrap(),

            _ => (),
        };
    }

    fn resize_event(&mut self, ctx: &mut Context, width: u32, height: u32) {
        // Handle window resizes
        let mut screen_coords = graphics::get_screen_coordinates(ctx);
        screen_coords.w = width as f32;
        screen_coords.h = height as f32;
        graphics::set_screen_coordinates(ctx, screen_coords).unwrap();
    }
}

fn world_to_screen(ctx: &Context, p: &Point2) -> Point2 {
    let (screen_w, screen_h) = graphics::get_size(ctx);
    let w = screen_w as f32;
    let h = screen_h as f32;

    let x = p.x + w / 2.0;
    let y = -(p.y + h / 2.0);

    Point2::new(x, y)
}

fn screen_to_world(ctx: &Context, p: &Point2) -> Point2 {
    let (screen_w, screen_h) = graphics::get_size(ctx);
    let w = screen_w as f32;
    let h = screen_h as f32;

    let x = p.x - w / 2.0;
    let y = -p.y - h / 2.0;

    Point2::new(x, y)
}

pub fn main() {

    let mut c = conf::Conf::new();
    c.window_setup.title = "Convex Hull".to_owned();
    c.window_setup.resizable = true;

    let ctx = &mut Context::load_from_conf("convex_hull", "Jonas HE", c).unwrap();
    let state = &mut State::new(ctx).unwrap();

    event::run(ctx, state).unwrap();
}

#[cfg(test)]
mod tests {
    use get_line_function;
    use is_left_of;
    use nth;
    use Point2;

    #[test]
    fn test_get_line_function() {
        let p1 = Point2::new(1.0, 7.0);
        let p2 = Point2::new(7.0, 4.0);

        let f1 = get_line_function(&p1, &p2).unwrap();
        let f2 = get_line_function(&p2, &p1).unwrap();

        assert_eq!(f1(0.0), f2(0.0));
    }

    #[test]
    fn test_is_left_of() {
        let p1 = Point2::new(1.0, 7.0);
        let p2 = Point2::new(7.0, 4.0);
        let oth = Point2::new(3.0, 10.0);

        assert!(is_left_of(&p1, &p2, &oth));

        assert!(!is_left_of(&p1, &oth, &p2));

        let p1 = Point2::new(2.0, 1.0);
        let p2 = Point2::new(1.0, 2.0);
        let oth = Point2::new(3.0, 1.0);
        assert!(!is_left_of(&p1, &p2, &oth));
    }

    #[test]
    fn test_nth() {
        let p1 = Point2::new(1.0, 7.0);
        let p2 = Point2::new(7.0, 4.0);
        let p3 = Point2::new(3.0, 0.0);
        let p4 = Point2::new(5.0, 9.0);

        assert_eq!(nth(&[p1, p2, p3, p4], 9), &p2);

        assert_eq!(nth(&[p1, p2, p3, p4], -9), &p4);
    }

}
