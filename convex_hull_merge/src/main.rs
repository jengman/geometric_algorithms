extern crate ggez;
extern crate rand;

use std::fs::File;
use std::io::{self, Read};

use ggez::conf;
use ggez::event;
use ggez::graphics::{self, DrawMode, Point2};
use ggez::{Context, GameResult};
use rand::random;

///////////////
// ALGORITHM //
///////////////

fn get_line_function(p1: &Point2, p2: &Point2) -> Option<Box<Fn(f32) -> f32>> {
    // Generates a function which describes the line from p1 to p2 as y(x) = kx+m
    if p1.x == p2.x {
        return None;
    }

    let k = (p2.y - p1.y) / (p2.x - p1.x);
    let m = p1.y - k * p1.x;

    Some(Box::new(move |x| k * x + m))
}

fn is_left_of(p1: &Point2, p2: &Point2, other: &Point2) -> bool {
    // Checks whether next is a left turn based on two points making up a line
    // The order of p1 and p2 is important!

    match (get_line_function(&p1, &p2), p1.x < p2.x) {
        (Some(f), true) => f(other.x) < other.y,

        (Some(f), false) => f(other.x) > other.y,

        (None, _) => p1.x < other.x,
    }
}

fn actual_index(set_size: usize, index: isize) -> usize {
    let size = set_size as isize;
    let mut i = index;

    while i < 0 {
        i += size;
    }

    (i % size) as usize
}

fn nth<'a, T>(slc: &'a [T], index: isize) -> &'a T {
    // Takes a wrapping index such as -1 or slc.len()+1 and returns the index as if it wrapped

    &slc[actual_index(slc.len(), index)]
}

fn determine_tangent_indices(t: &[Point2], o: &[Point2]) -> (isize, isize) {
    // Pointers to this' and other's current tangent vertice
    let mut pt: isize = 0;
    let mut po: isize = 0;

    // Move o pointer to correctish place
    while {
        let oh = nth(o, po - 1);
        let oi = nth(o, po);
        let oj = nth(o, po + 1);

        let ti = nth(t, pt);

        // Check if the other's neighbors are on the right side
        !is_left_of(oi, ti, oh) || !is_left_of(oi, ti, oj)
    } {
        // Move t pointer to next clockwise
        po += 1;
    }

    // Move t pointer to correct place
    while {
        let th = nth(t, pt - 1);
        let ti = nth(t, pt);
        let tj = nth(t, pt + 1);

        let oi = nth(o, po);

        is_left_of(ti, oi, th) || is_left_of(ti, oi, tj)
    } {
        // Move t pointer to next clockwise if th or tj is left of tangent line
        pt += 1;
    }

    {
        //
        let oi = nth(o, po);
        let oj = nth(o, po + 1);

        let ti = nth(t, pt);

        if !is_left_of(oi, ti, oj) {
            // TODO: Test if this really is needed
            po += 1;
        }
    }

    (pt, po)
}

fn merge_disjoint_hulls(h1: &[Point2], h2: &[Point2]) -> Vec<Point2> {
    // Tangent A indices
    let (ta1, ta2) = determine_tangent_indices(h1, h2);

    // Tangent B indices
    let (tb2, tb1) = determine_tangent_indices(h2, h1);

    let mut hull = vec![
        h1[actual_index(h1.len(), ta1)],
        h2[actual_index(h2.len(), ta2)],
    ];

    let mut i = ta2 + 1;

    while i != tb2 {
        hull.push(h2[actual_index(h2.len(), i)]);

        i += 1;
    }

    hull.push(h2[actual_index(h2.len(), tb2)]);
    hull.push(h1[actual_index(h1.len(), tb1)]);

    i = tb1 + 1;

    while actual_index(h1.len(), i) != actual_index(h1.len(), ta1) {
        hull.push(h1[actual_index(h1.len(), i)]);

        i += 1;
    }

    hull.push(h1[actual_index(h1.len(), ta1)]);

    hull
}

///////////////
//    GUI    //
///////////////

struct State {
    verts: Vec<Point2>,
    lines: Vec<Point2>,
    instr: [graphics::Text; 4],
}

impl State {
    fn new(ctx: &mut Context) -> GameResult<State> {
        let font = graphics::Font::default_font()?;

        let instr = [
            graphics::Text::new(ctx, "[E]xec algorithm", &font)?,
            graphics::Text::new(ctx, "[R]andomize", &font)?,
            graphics::Text::new(ctx, "[L]oad verts", &font)?,
            graphics::Text::new(ctx, "[C]lear", &font)?,
        ];

        let s = State {
            verts: Vec::new(),
            lines: Vec::new(),
            instr: instr,
        };

        Ok(s)
    }
}

impl event::EventHandler for State {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);

        for v in &self.verts {
            let p = world_to_screen(ctx, v);
            graphics::circle(ctx, DrawMode::Fill, p, 2.0, 0.1)?;
        }

        // Draw green lines
        graphics::set_color(ctx, graphics::Color::new(0.1, 0.8, 0.5, 1.0))?;
        if self.lines.len() > 1 {
            let lines: Vec<_> = self.lines.iter().map(|p| world_to_screen(ctx, p)).collect();
            graphics::line(ctx, &lines, 2.0)?;
        }

        // Reset to white
        graphics::set_color(ctx, graphics::Color::new(1.0, 1.0, 1.0, 1.0))?;

        let mut y_offset = 4.0;
        for e in self.instr.iter() {
            let pos = graphics::Point2::new(4.0, y_offset);
            graphics::draw(ctx, e, pos, 0.0)?;

            y_offset += e.height() as f32 + 2.0;
        }

        graphics::present(ctx);
        Ok(())
    }

    fn mouse_button_down_event(
        &mut self,
        ctx: &mut Context,
        button: event::MouseButton,
        x: i32,
        y: i32,
    ) {
        if button == event::MouseButton::Left {
            let p = screen_to_world(ctx, &Point2::new(x as f32, y as f32));
            self.verts.push(p);
        }
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: event::Keycode,
        _mod: event::Mod,
        _rpt: bool,
    ) {
        match keycode {
            event::Keycode::E => {
                self.lines = vec![]; //calculate(&mut self.verts);
            }

            event::Keycode::C => {
                self.verts = Vec::new();
                self.lines = Vec::new();
            }

            event::Keycode::R => {
                let (screen_w, screen_h) = graphics::get_size(ctx);
                let w = screen_w as f32;
                let h = screen_h as f32;

                let mut verts: Vec<_> = (0..)
                    .take(20)
                    .map(|_| {
                        (
                            random::<f32>() * w * 0.8 + w * 0.1,
                            random::<f32>() * h * 0.8 + h * 0.1,
                        )
                    })
                    .map(|(x, y)| screen_to_world(ctx, &Point2::new(x, y)))
                    .collect();

                self.verts.append(&mut verts);
                self.lines = Vec::new();
            }

            event::Keycode::L => {
                let (w, h) = graphics::get_size(ctx);

                let verts = read_vertices("input.dat").expect("Could not read `input.dat` file");

                for vert in verts {
                    self.verts.push(screen_to_world(
                        ctx,
                        &Point2::new(vert.x + (w as f32) / 2.0, vert.y + (h as f32) / 2.0),
                    ));
                }
                self.lines = Vec::new();
            }

            event::Keycode::Q => ctx.quit().unwrap(),

            event::Keycode::Escape => ctx.quit().unwrap(),

            _ => (),
        };
    }

    fn resize_event(&mut self, ctx: &mut Context, width: u32, height: u32) {
        // Handle window resizes
        let mut screen_coords = graphics::get_screen_coordinates(ctx);
        screen_coords.w = width as f32;
        screen_coords.h = height as f32;
        graphics::set_screen_coordinates(ctx, screen_coords).unwrap();
    }
}

fn world_to_screen(ctx: &Context, p: &Point2) -> Point2 {
    let (screen_w, screen_h) = graphics::get_size(ctx);
    let w = screen_w as f32;
    let h = screen_h as f32;

    let x = p.x + w / 2.0;
    let y = -(p.y + h / 2.0);

    Point2::new(x, y)
}

fn screen_to_world(ctx: &Context, p: &Point2) -> Point2 {
    let (screen_w, screen_h) = graphics::get_size(ctx);
    let w = screen_w as f32;
    let h = screen_h as f32;

    let x = p.x - w / 2.0;
    let y = -p.y - h / 2.0;

    Point2::new(x, y)
}

fn read_vertices(fname: &str) -> io::Result<Vec<Point2>> {
    let mut file = File::open(fname)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let verts: Vec<_> = contents
        .lines()
        .map(|line| line.split(' ').collect::<Vec<_>>())
        .map(|pair| {
            Point2::new(
                pair[0].parse::<f32>().expect("Could not parse float"),
                pair[1].parse::<f32>().expect("Could not parse float"),
            )
        })
        .collect();
    Ok(verts)
}

pub fn main() {
    let (verts, merged) = {
        let p0 = Point2::new(350.0, 390.0);
        let p1 = Point2::new(400.0, 360.0);
        let p2 = Point2::new(400.0, 330.0);
        let p3 = Point2::new(330.0, 310.0);
        let p4 = Point2::new(310.0, 370.0);

        let hull1 = [p4, p0, Point2::new(370.0, 340.0), p3];

        let hull2 = [Point2::new(380.0, 330.0), p1, p2];

        let hull = merge_disjoint_hulls(&hull1, &hull2);

        let mut verts = Vec::new();
        verts.extend_from_slice(&hull1);
        verts.extend_from_slice(&hull2);

        (verts, hull)
    };

    let mut c = conf::Conf::new();
    c.window_setup.title = "Convex Hull".to_owned();
    c.window_setup.resizable = true;

    let ctx = &mut Context::load_from_conf("convex_hull", "Jonas HE", c).unwrap();
    let state = &mut State::new(ctx).unwrap();

    state.verts = verts.iter().map(|v| screen_to_world(ctx, v)).collect();
    state.lines = merged.iter().map(|v| screen_to_world(ctx, v)).collect();

    event::run(ctx, state).unwrap();
}

#[cfg(test)]
mod tests {
    use determine_tangent_indices;
    use get_line_function;
    use is_left_of;
    use merge_disjoint_hulls;
    use nth;
    use Point2;

    #[test]
    fn test_get_line_function() {
        let p1 = Point2::new(1.0, 7.0);
        let p2 = Point2::new(7.0, 4.0);

        let f1 = get_line_function(&p1, &p2).unwrap();
        let f2 = get_line_function(&p2, &p1).unwrap();

        assert_eq!(f1(0.0), f2(0.0));
    }

    #[test]
    fn test_is_left_of() {
        let p1 = Point2::new(1.0, 7.0);
        let p2 = Point2::new(7.0, 4.0);
        let oth = Point2::new(3.0, 10.0);

        assert!(is_left_of(&p1, &p2, &oth));

        assert!(!is_left_of(&p1, &oth, &p2));

        let p1 = Point2::new(2.0, 1.0);
        let p2 = Point2::new(1.0, 2.0);
        let oth = Point2::new(3.0, 1.0);
        assert!(!is_left_of(&p1, &p2, &oth));
    }

    #[test]
    fn test_nth() {
        let p1 = Point2::new(1.0, 7.0);
        let p2 = Point2::new(7.0, 4.0);
        let p3 = Point2::new(3.0, 0.0);
        let p4 = Point2::new(5.0, 9.0);

        assert_eq!(nth(&[p1, p2, p3, p4], 9), &p2);

        assert_eq!(nth(&[p1, p2, p3, p4], -9), &p4);
    }

    #[test]
    fn test_determine_tangent_indices() {
        let hull1 = [
            Point2::new(1.0, 7.0),
            Point2::new(5.0, 9.0),
            Point2::new(7.0, 4.0),
            Point2::new(3.0, 1.0),
        ];

        let hull2 = [
            Point2::new(8.0, 3.0),
            Point2::new(10.0, 6.0),
            Point2::new(10.0, 3.0),
        ];

        assert_eq!(determine_tangent_indices(&hull1, &hull2), (1, 1));
        assert_eq!(determine_tangent_indices(&hull2, &hull1), (2, 3));
    }

    #[test]
    fn test_merge_disjoint_hulls() {
        let p0 = Point2::new(5.0, 9.0);
        let p1 = Point2::new(10.0, 6.0);
        let p2 = Point2::new(10.0, 3.0);
        let p3 = Point2::new(3.0, 1.0);
        let p4 = Point2::new(1.0, 7.0);

        let hull1 = [p4, p0, Point2::new(7.0, 4.0), p3];

        let hull2 = [Point2::new(8.0, 3.0), p1, p2];

        let hull = merge_disjoint_hulls(&hull1, &hull2);

        assert_eq!(hull, vec![p0, p1, p2, p3, p4]);
    }
}
