
from random import randint

def load(fname):
    points = []
    with open(fname) as fp:
        for line in iter(fp.readline, ''):
            x, y = line.split(' ')
            points.append((float(x), float(y)))

    return points

def rand(amt):
    pairs = []
    for i in range(amt):
        x = randint(-300, 300)
        y = randint(-290, 290)
        pairs.append((x, y))

    return pairs

