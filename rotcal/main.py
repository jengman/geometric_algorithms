import kivy, sys, math, random

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
from kivy.graphics import Rectangle, Color, Ellipse, Line

import algo
from util import rand, load

class MenuWidget(Widget):

    def load(self):
        verts = load("vertices.txt")

        min_x = min(verts)[0]
        max_x = max(verts)[0]
        min_y = min(verts, key=lambda x: x[1])[1]
        max_y = max(verts, key=lambda x: x[1])[1]

        unit_x = (232.5 / (max_x - min_x))
        unit_y = (225 / (max_y - min_y))
        normalized = map(lambda v: (v[0] * unit_x, v[1] * unit_y), verts)

        self.parent.vertices += normalized
        self.parent.redraw()

    def random(self):
        vertices = rand(20)
        for v in vertices:
            self.parent.vertices.append(v)
        self.parent.redraw()

    def clear(self):
        self.parent.clear()

    def calculate(self):
        if len(self.parent.vertices) < 3: return
        helpers, lines = algo.rotating_calipers(self.parent.vertices)
        self.parent.helpers = helpers
        self.parent.lines = lines
        self.parent.redraw()

class DrawWidget(Widget):

    def on_touch_down(self, touch):
        if touch.x < 180: return

        x = touch.x - 490
        y = touch.y - 300
        self.parent.vertices += [(x, y)]
        self.parent.redraw()

class UILayout(BoxLayout):
    vertices = []
    helpers = []
    lines = []

    def __init__(self, **kwargs):
        super(UILayout, self).__init__(**kwargs)
        h = Window._get_height()

        self.add_widget(MenuWidget(size_hint=(None, None),
            size=(180, h)))
        self.add_widget(DrawWidget())

    def redraw(self):
        # Canvas size: 620x600
        self.canvas.remove_group('vertices')

        # First dot
        with self.canvas:
            Color(.1, .4, .1)
            d = 8.
            r = d/2
            for v in self.vertices[:1]:
                x = v[0]-r + 490
                y = v[1]-r + 300
                Ellipse(pos=(x, y), size=(d, d), group="vertices")

        # Dots
        with self.canvas:
            Color(.1, .1, .1)
            d = 7.
            r = d/2
            for v in self.vertices[1:]:
                x = v[0]-r + 490
                y = v[1]-r + 300
                Ellipse(pos=(x, y), size=(d, d), group="vertices")

        # Lines
        with self.canvas:
            Color(.9, .1, .2)
            d = 2.
            r = d/2
            for l in range(len(self.lines))[1:]:
                x1 = self.lines[l-1][0] + 490 - r
                y1 = self.lines[l-1][1] + 300 - r
                x2 = self.lines[l][0] + 490 - r
                y2 = self.lines[l][1] + 300 - r

                Line(points=(x1, y1, x2, y2), width=d,
                    group="vertices")

        # Helpers
        for element in self.helpers:
            with self.canvas:
                Color(random.random(), random.random(), random.random())
                d = 2.
                r = d/2
                for l in range(len(element)):
                    x1 = element[l-1][0] + 490 - r
                    y1 = element[l-1][1] + 300 - r
                    x2 = element[l][0] + 490 - r
                    y2 = element[l][1] + 300 - r

                    Line(points=(x1, y1, x2, y2), width=d,
                        group="vertices")

    def clear(self):
        self.vertices = []
        self.helpers = []
        self.lines = []
        self.canvas.remove_group('vertices')

class UIApp(App):
    def build(self):
        Window._set_clearcolor((.9, .9, .9, 1))
        return UILayout()

if __name__ == "__main__":
    sys.setrecursionlimit(1500)
    UIApp().run()

