import math
import numpy as np
np.tau = 2*np.pi

def merge(a, b):
    if len(a) < 1: return b
    if len(b) < 1: return a

    ha, ta = a[0], a[1:]
    hb, tb = b[0], b[1:]

    if ha[0] == hb[0]:
        if ha[1] > hb[1]: # Sort in reverse order for Y
            return (ha,) + merge(ta, b)
        else:
            return (hb,) + merge(a, tb)

    elif ha[0] < hb[0]:
        return (ha,) + merge(ta, b)
    else:
        return (hb,) + merge(a, tb)

def merge_sort(tup):
    if len(tup) == 1: return tup

    a = merge_sort( tup[ : int(len(tup)/2) ] )
    b = merge_sort( tup[ int(len(tup)/2) : ] )
    return merge(a, b)

def flip(tup):
    if len(tup) < 1: return tup

    x, xs = tup[0], tup[1:]
    x = (x[0], -x[1])

    return (x,) + flip(xs)

def rev(tup):
    if len(tup) < 1: return tup

    x, xs = tup[:1], tup[1:]
    return rev(xs) + x

def get_straight_line(p1, p2):
    if p1[0] == p2[0]:
        return None # y = infinity

    k = float(p2[1]-p1[1]) / (p2[0]-p1[0]) # deltaY / deltaX
    m = p1[1] - k*p1[0]
    return lambda x: k*x + m

def check_left_turn(stack, nxt):
    if len(stack) < 2: return False
    p1 = stack[-2]
    p2 = stack[-1]
    f = get_straight_line(p1, p2) # Straight line equation
    if f == None: # Same X value
        if p1[1] > p2[1]:
            return True
        return False

    return f(nxt[0]) < nxt[1]

def upper_hull(stack, nxt):
    # Exclude last element from current stack if left turn
    if check_left_turn(stack, nxt):
        return upper_hull(stack[:-1], nxt)

    return stack + (nxt,)

def andrews(l):
    points = merge_sort(tuple(l))
    upper = (points[0],)
    for p in points[1:]:
        upper = upper_hull(upper, p)

    flipped = flip(points)
    lower = (flipped[0],)
    for p in flipped[1:]:
        lower = upper_hull(lower, p)
    lower = rev(flip(lower))

    return upper + lower[1:]

class Caliper(object):
    def __init__(self, hull, index, angle):
        self.hull = hull
        self.index = index
        self.angle = angle

    def point(self):
        return np.asarray(self.hull[self.index % len(self.hull)])

    def angle_to_next_point(self):
        p1 = self.point()
        p2 = np.asarray(self.hull[(self.index + 1) % len(self.hull)])

        d = p2 - p1
        angle = np.arctan2(d[1], d[0])
        return (angle + np.tau) % np.tau # Positive 0 < angle < 2PI

    def get_delta_angle(self):
        next_angle = self.angle_to_next_point()
        delta = next_angle - self.angle
        return (delta + np.tau) % np.tau

    def get_constant(self):
        p = self.point()
        return p[1] - (self.get_slope() * p[0])

    def get_slope(self):
        return np.tan(self.angle)

    def vertical(self):
        return np.isclose(abs(self.angle) % np.pi, np.pi/2)

    def horizontal(self):
        return np.isclose(abs(self.angle) % np.pi, 0)

    def get_intersection(self, other):
        selfp = self.point()
        otherp = other.point()
        x, y = None, None

        if self.vertical():
            x = selfp[0]
        elif self.horizontal():
            x = otherp[0]
        else:
            x = (other.get_constant() - self.get_constant()) / (self.get_slope() - other.get_slope())

        if self.vertical():
            y = other.get_constant()
        elif self.horizontal():
            y = self.get_constant()
        else:
            y = (self.get_slope() * x) + self.get_constant()

        return np.array([x, y])

    def rotate(self, angle):
        if np.isclose(self.get_delta_angle(), angle):
            self.index += 1

        self.angle += angle

def get_index(hull, corner):
    index = 0
    p = hull[index]

    for i in range(1, len(hull)-1):
        t = hull[i]
        update = False

        if corner == "upper right":
            update = t[0]>p[0] or (np.isclose(t,p)[0] and t[1]>p[1])
        elif corner == "upper left":
            update = t[1]>p[1] or (np.isclose(t,p)[1] and t[0]<p[0])
        elif corner == "lower left":
            update = t[0]<p[0] or (np.isclose(t,p)[0] and t[1]<p[1])
        elif corner == "lower right":
            update = t[1]<p[1] or (np.isclose(t,p)[1] and t[0]>p[0])

        if update:
            p = t
            index = i

    return index

def smallest_angle(c1, c2, c3, c4):
    a1 = c1.get_delta_angle()
    a2 = c2.get_delta_angle()
    a3 = c3.get_delta_angle()
    a4 = c4.get_delta_angle()

    return min(a1, a2, a3, a4)

def bounding_rectangles(points):
    rectangles = []

    A = Caliper(points, get_index(points, "upper right"), 0.5*np.pi)
    B = Caliper(points, get_index(points, "upper left"), np.pi)
    C = Caliper(points, get_index(points, "lower left"), 1.5*np.pi)
    D = Caliper(points, get_index(points, "lower right"), 0)

    while D.angle < 0.5*np.pi:
        rectangles.append([
            A.get_intersection(B),
            B.get_intersection(C),
            C.get_intersection(D),
            D.get_intersection(A)
        ])

        min_angle = smallest_angle(A, B, C, D)

        A.rotate(min_angle)
        B.rotate(min_angle)
        C.rotate(min_angle)
        D.rotate(min_angle)

    return rectangles

def area(rect):
    len1 = np.linalg.norm(rect[0] - rect[1])
    len2 = np.linalg.norm(rect[1] - rect[2])
    return len1 * len2

def rotating_calipers(l):
    hull = andrews(l)
    hull = list(hull)
    hull.reverse()
    rectangles = bounding_rectangles(hull)

    min_rect = rectangles[0]
    for rect in rectangles:
        if area(rect) < area(min_rect):
            min_rect = rect

    min_rect = map(lambda p: list(p), min_rect)
    lines = min_rect + [min_rect[0]]
    helpers = []

    return (helpers, lines)

