import math, random

class Vec2:
    x = None
    y = None
    def __init__(self, p):
        self.x = p[0]
        self.y = p[1]
    def __repr__(self):
        return "[" +str(self.x)+ ", " +str(self.y)+ "]"
    def __str__(self):
        return "[" +str(self.x)+ ", " +str(self.y)+ "]"
    def length(self):
        return math.sqrt(self.x**2 + self.y**2)
    def to_tuple(self):
        return ( self.x, self.y )

def get_center(C):
    if len(C) == 2:
        a, b = C
        return Vec2(( (b.x + a.x) / 2., (b.y + a.y) / 2. ))

    if len(C) == 3:
        a, b, c = C

        if a.y == b.y and b.y == c.y: # All y same
            mx = max(C, key=lambda p: p.x)
            mn = min(C, key=lambda p: p.x)
            return get_center((mn, mx))

        if a.x == b.x and b.x == c.x: # All x same
            mx = max(C, key=lambda p: p.y)
            mn = min(C, key=lambda p: p.y)
            return get_center((mn, mx))

        if a.x == b.x:
            ab_normal = 0
        elif a.y == b.y:
            return get_center(C[1:] + C[:1])
        else:
            ab_slope = (b.y - a.y) / float(b.x - a.x)
            ab_normal = - 1/ab_slope

        if b.x == c.x:
            bc_normal = 0
        elif b.y == c.y:
            return get_center(C[1:] + C[:1])
        else:
            bc_slope = (c.y - b.y) / float(c.x - b.x)
            bc_normal = - 1/bc_slope

        if ab_normal == bc_normal: # Parallell
            return get_center(C[1:] + C[:1])

        ab_center = get_center((a, b))
        bc_center = get_center((b, c))
        ab_0 = ab_center.y - ab_normal*ab_center.x
        bc_0 = bc_center.y - bc_normal*bc_center.x

        x = (bc_0 - ab_0) / (ab_normal - bc_normal)
        y = ab_normal * x + ab_0

        return Vec2(( x, y ))

def radius(C):
    if len(C) == 2:
        a, b = C
        return Vec2(( b.x - a.x, b.y - a.y )).length() / 2

    elif len(C) == 3:
        center = get_center(C)
        p = C[0]
        return Vec2(( p.x - center.x, p.y - center.y)).length()

def outside(p, C):
    r = radius(C)
    c = get_center(C)
    return Vec2(( p.x - c.x, p.y - c.y )).length() >= r

def brute_force(l):
    Cmin = None

    for a in l:
        for b in [x for x in l if x != a]:
            p_outside = False
            C = (Vec2(a), Vec2(b))
            for d in [x for x in l if not x in (a, b)]:
                if outside(Vec2(d), C):
                    p_outside = True
                    break

            if p_outside: continue

            if Cmin == None:
                Cmin = C
            elif radius(C) < radius(Cmin):
                Cmin = C

    for a in l:
        for b in [x for x in l if x != a]:
            for c in [x for x in l if not x in (a, b)]:
                p_outside = False
                C = (Vec2(a), Vec2(b), Vec2(c))
                for d in [x for x in l if not x in (a, b, c)]:
                    if outside(Vec2(d), C):
                        p_outside = True
                        break

                if p_outside: continue

                if Cmin == None:
                    Cmin = C
                elif radius(C) < radius(Cmin):
                    Cmin = C

    return (get_center(Cmin).to_tuple(), radius(Cmin))


def mini_disc(l):
    # Randomize order
    random.shuffle(l)

    Cmin = ( Vec2(l[0]), Vec2(l[1]) )
    for i in range(2, len(l)):
        idx = len(l) - i
        if outside(Vec2(l[i]), Cmin):
            # MiniDiscWithPoint
            Cmin = mini_disc_one_point(l[:i], l[i])
    return (get_center(Cmin).to_tuple(), radius(Cmin))

def mini_disc_one_point(l, p):
    # Randomize order
    random.shuffle(l)

    Cmin = ( Vec2(p), Vec2(l[0]) )
    for i in range(1, len(l)):
        if outside(Vec2(l[i]), Cmin):
            # MiniDiscWith2Points
            Cmin = mini_disc_two_points(l[:i], l[i], p)
    return Cmin

def mini_disc_two_points(l, p, q):
    Cmin = ( Vec2(p), Vec2(q) )
    for i in range(len(l)):
        if outside(Vec2(l[i]), Cmin):
            Cmin = ( Vec2(p), Vec2(q), Vec2(l[i]) )
    return Cmin

