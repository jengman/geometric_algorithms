extern crate ggez;
extern crate rand;

use std::cmp::Ordering::Equal;
use std::fs::File;
use std::io::{self, Read};

use ggez::conf;
use ggez::event;
use ggez::graphics::{self, DrawMode, Point2};
use ggez::{Context, GameResult};
use rand::random;


fn get_line_function(p1: &Point2, p2: &Point2) -> Option<Box<Fn(f32) -> f32>> {
    // Generates a function which describes the line from p1 to p2 as y(x) = kx+m
    if p1.x == p2.x {
        return None;
    }

    let k = (p2.y - p1.y) / (p2.x - p1.x);
    let m = p1.y - k * p1.x;

    Some(Box::new(move |x| k * x + m))
}

fn is_left_turn(stack: &[Point2], next: &Point2) -> bool {
    // Checks whether next is a left turn based on the last points of the stack
    let slen = stack.len();
    if slen < 2 {
        return false;
    }

    let p1 = stack[slen - 2];
    let p2 = stack[slen - 1];

    match get_line_function(&p1, &p2) {
        Some(f) => f(next.x) < next.y,
        None => p1.y > p2.y,
    }
}

fn flip(verts: &[Point2]) -> Vec<Point2> {
    // Flips vertices around y=0
    verts.iter().map(|p| Point2::new(p.x, -p.y)).collect()
}

fn upper_hull(verts: &[Point2]) -> Vec<Point2> {
    let mut upper = vec![verts[0]];
    let mut viter = verts.iter();
    let mut next = viter.next();

    while next.is_some() {
        let nextv = next.unwrap();

        if is_left_turn(&upper, &nextv) {
            upper.pop();
        } else {
            upper.push(*nextv);
            next = viter.next();
        }
    }

    upper
}

fn calculate(verts: &mut [Point2]) -> Vec<Point2> {
    if verts.len() < 2 {
        return Vec::new();
    } else {
        println!("Calculating hull of {} points!", verts.len());
    }

    verts.sort_by(|&a, &b| a.x.partial_cmp(&b.x).unwrap_or(Equal));

    // Upper half of hull
    let mut hull = upper_hull(verts);

    let mut hull_segment = upper_hull(&flip(&verts));
    // Flip back
    hull_segment = flip(&hull_segment);
    hull_segment.reverse();


    hull.extend(hull_segment);
    hull
}

struct State {
    verts: Vec<Point2>,
    lines: Vec<Point2>,
    instr: [graphics::Text; 4],
}

impl State {
    fn new(ctx: &mut Context) -> GameResult<State> {
        let font = graphics::Font::default_font()?;

        let instr = [
            graphics::Text::new(ctx, "[E]xec algorithm", &font)?,
            graphics::Text::new(ctx, "[R]andomize", &font)?,
            graphics::Text::new(ctx, "[L]oad verts", &font)?,
            graphics::Text::new(ctx, "[C]lear", &font)?,
        ];

        let s = State {
            verts: Vec::new(),
            lines: Vec::new(),
            instr: instr,
        };

        Ok(s)
    }
}

impl event::EventHandler for State {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);

        for v in &self.verts {
            let p = world_to_screen(ctx, v);
            graphics::circle(ctx, DrawMode::Fill, p, 2.0, 0.1)?;
        }

        // Draw green lines
        graphics::set_color(ctx, graphics::Color::new(0.1, 0.8, 0.5, 1.0))?;
        if self.lines.len() > 1 {
            let lines: Vec<_> = self.lines.iter().map(|p| world_to_screen(ctx, p)).collect();
            graphics::line(ctx, &lines, 2.0)?;
        }

        // Reset to white
        graphics::set_color(ctx, graphics::Color::new(1.0, 1.0, 1.0, 1.0))?;

        let mut y_offset = 4.0;
        for e in self.instr.iter() {
            let pos = graphics::Point2::new(4.0, y_offset);
            graphics::draw(ctx, e, pos, 0.0)?;

            y_offset += e.height() as f32 + 2.0;
        }

        graphics::present(ctx);
        Ok(())
    }

    fn mouse_button_down_event(
        &mut self,
        ctx: &mut Context,
        button: event::MouseButton,
        x: i32,
        y: i32,
    ) {
        if button == event::MouseButton::Left {
            let p = screen_to_world(ctx, &Point2::new(x as f32, y as f32));
            self.verts.push(p);
        }
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: event::Keycode,
        _mod: event::Mod,
        _rpt: bool,
    ) {

        match keycode {
            event::Keycode::E => {
                self.lines = calculate(&mut self.verts);
            }

            event::Keycode::C => {
                self.verts = Vec::new();
                self.lines = Vec::new();
            }

            event::Keycode::R => {
                let (screen_w, screen_h) = graphics::get_size(ctx);
                let w = screen_w as f32;
                let h = screen_h as f32;

                let mut verts: Vec<_> = (0..)
                    .take(20)
                    .map(|_| {
                        (
                            random::<f32>() * w * 0.8 + w * 0.1,
                            random::<f32>() * h * 0.8 + h * 0.1,
                        )
                    })
                    .map(|(x, y)| screen_to_world(ctx, &Point2::new(x, y)))
                    .collect();

                self.verts.append(&mut verts);
                self.lines = Vec::new();
            }

            event::Keycode::L => {
                let (w, h) = graphics::get_size(ctx);

                let verts = read_vertices("input.dat").expect("Could not read `input.dat` file");

                for vert in verts {
                    self.verts.push(Point2::new(
                        vert.x + (w as f32) / 2.0,
                        vert.y + (h as f32) / 2.0,
                    ));
                }
                self.lines = Vec::new();
            }

            event::Keycode::Q => ctx.quit().unwrap(),

            event::Keycode::Escape => ctx.quit().unwrap(),

            _ => (),
        };
    }

    fn resize_event(&mut self, ctx: &mut Context, width: u32, height: u32) {
        // Handle window resizes
        let mut screen_coords = graphics::get_screen_coordinates(ctx);
        screen_coords.w = width as f32;
        screen_coords.h = height as f32;
        graphics::set_screen_coordinates(ctx, screen_coords).unwrap();
    }
}

fn world_to_screen(ctx: &Context, p: &Point2) -> Point2 {
    let (screen_w, screen_h) = graphics::get_size(ctx);
    let w = screen_w as f32;
    let h = screen_h as f32;

    let x = p.x + w / 2.0;
    let y = -(p.y + h / 2.0);

    Point2::new(x, y)
}

fn screen_to_world(ctx: &Context, p: &Point2) -> Point2 {
    let (screen_w, screen_h) = graphics::get_size(ctx);
    let w = screen_w as f32;
    let h = screen_h as f32;

    let x = p.x - w / 2.0;
    let y = -p.y - h / 2.0;

    Point2::new(x, y)
}

fn read_vertices(fname: &str) -> io::Result<Vec<Point2>> {
    let mut file = File::open(fname)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let verts: Vec<_> = contents
        .lines()
        .map(|line| line.split(' ').collect::<Vec<_>>())
        .map(|pair| {
            Point2::new(
                pair[0].parse::<f32>().expect("Could not parse float"),
                pair[1].parse::<f32>().expect("Could not parse float"),
            )
        })
        .collect();
    Ok(verts)
}

pub fn main() {
    let mut c = conf::Conf::new();
    c.window_setup.title = "Convex Hull".to_owned();
    c.window_setup.resizable = true;

    let ctx = &mut Context::load_from_conf("convex_hull", "Jonas HE", c).unwrap();
    let state = &mut State::new(ctx).unwrap();

    event::run(ctx, state).unwrap();
}
